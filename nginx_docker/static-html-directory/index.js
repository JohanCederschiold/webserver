Vue.component('thecontent', {
  template: `<div>
              <div class="container is-desktop">
              <div class="content is-medium">
              <div class="columns">
              <div class="column"></div>
              <div class="column is-three-quarters">
              <mynavbar></mynavbar>
              <router-view></router-view>
              </div>
              <div class="column"></div>
              </div>
              </div>
              </div>
             </div>`
})


const about = Vue.component('about', {
  created() {
    this.calculateAge(),
    // this.$store.state.currentChoice=true
    this.$store.commit('showingContent', true ),
    this.getLatestKms()
  },
  data() {
    return {
        age: null,
        kmsArray: null,
        kms: null,
        modalVisible: 'modal'

    }
  },
  methods: {
    calculateAge(){
      let today = new Date()
      let birthday = new Date('1975-08-07')
      let livedFor = Math.floor((today - birthday) / (1000 * 60 * 60 * 24 * 365))
      this.age = livedFor
    },
    getLatestKms(){
      fetch('http://95.217.9.10/api').then(response => response.json()).then(result => { this.kmsArray = result})
    },
    getLatestDistance(){
      let greatestDistance = 0;
      for (let i = 0; i < this.kmsArray.length; i++ ) {
        if (Number(this.kmsArray[i].Distance) > greatestDistance ) {
          greatestDistance = Number(this.kmsArray[i].Distance)
        }
      }
      this.kms = greatestDistance
    },
    toggleModal(){
      this.getLatestDistance()
      if (this.modalVisible === 'modal') {
        this.modalVisible = 'modal is-active'
      } else {
        this.modalVisible = 'modal'
      }
    }
  },
  template: `
            <div>
              <section class="hero is-info">
                <div class="hero-body">
                  <div class="container">
                    <h1 class="title">
                      About
                    </h1>
                    <h2 class="subtitle">
                      Short background
                    </h2>
                  </div>
                </div>
              </section>
                <p>I'm {{age}} years old and live in the east part of Göteborg with my wife and two kids.</p>
                <p> Currently, I’m pursuing my, previously dormant, interest in programming. I’ve taken a leave of absence to enroll in Yrgos’ two-year program (Java Enterprise Developer). For more information on what we study, see Tools, Languages or Development Frameworks in the menu (or visit the
                  <a href="https://yrgo.se/utbildningar/teknik/java-enterprise-utvecklare/">homepage of the program</a>).</p>
                <p>When I’m not studying, I like to spend time with my family, to exercise (mainly running or <span v-on:click="toggleModal" class="has-text-link">biking</span>) or to spend my time taking photos (first and foremost <a href="https://www.instagram.com/JuanProtector/">nature photography</a>).</p>
                <div v-bind:class="modalVisible">
                  <div class="modal-background"></div>
                  <div class="modal-content">
                  <section class="modal-card-body">
                    <div>
                      My goal is to ride 5000 km in 2019. I'm currently
                      at {{kms}} kilometers.
                    </div>
                  </section>
                  </div>
                  <button v-on:click="toggleModal" class="modal-close is-large" aria-label="close"></button>
                </div>
                <p>If you’re more interested in my professional background, please visit my <a href="https://linkedin.com/in/johan-cederschiold">LinkedIn profile</a> or Download my <a href="CV2019.pdf" download="cv">brand-new CV</a>.</p>
             </div>`
})

const stats = Vue.component('stats', {
  created() {
    this.$store.commit('showingContent', true ),
    this.getLatestKms()
  },
  data() {
    return {
        kmsArray: null,
        kms: null,
        percentageYtd: null
    }
  },
  methods: {
    getLatestKms(){
      fetch('http://95.217.9.10/api').then(response => response.json()).then(result => { this.kmsArray = result}).then(result => this.getLatestDistance()).then(result => this.calculateDailyFeedback())
    },
    getDayOfYear() {

      const msInDay = 86400000
      // Calculate newYearsEve
      let thisYear = new Date(Date.now()).getFullYear()
      let newYearsEve = new Date(thisYear + '-01-01').getTime()

      // Todays date
      let today = new Date(Date.now()).getTime()

      // Calculate
      let daysYTD = Math.trunc((today - newYearsEve)/msInDay) + 1

      return daysYTD
    },
    calculateDailyFeedback() {
      const goal = 5000
      let atThisTime = this.kms /( goal/365 * this.getDayOfYear())
      this.percentageYtd = atThisTime
    },
    getLatestDistance(){
      let greatestDistance = 0;
      for (let i = 0; i < this.kmsArray.length; i++ ) {
        if (Number(this.kmsArray[i].Distance) > greatestDistance ) {
          greatestDistance = Number(this.kmsArray[i].Distance)
        }
      }
      this.kms = greatestDistance
    }
  },
  template: `
            <div>
            <section class="hero is-info">
              <div class="hero-body">
                <div class="container">
                  <h1 class="title">
                    Bike stats
                  </h1>
                  <h2 class="subtitle">
                    Current progress report
                  </h2>
                </div>
              </div>
            </section>
            <div class="tile is-ancestor">
              <div class="tile is-parent is-vertical">
                <article class="tile is-child notification is-warning">
                  <p class="title is-1">Weighted progress</p>
                  <p class="content is-small">100% indicates that current progress is in line
                    with the yearly goal of 5000 km.</p>
                  <p class="content is-large">{{ Math.round( percentageYtd * 100 ) }}%</p>
                </article>
                <article class="tile is-child notification is-info">
                  <p class="title is-1">Current progress</p>
                  <p class="content is-small">Total progress towards yearly goal of 5000 km.</p>
                  <p class="content is-large">{{ Math.round( (kms/5000) * 100 ) }}%</p>
                </article>
                </div>
                <div class="tile is-parent is-vertical">
                <article class="tile is-child notification is-danger">
                  <p class="title is-1">Current kilometers</p>
                  <p class="content is-small">The total sum of kilometers I've biked YTD.</p>
                  <p class="content is-large">{{ kms }} km</p>
                </article>
                <article class="tile is-child notification is-primary">
                  <p class="title is-1">Required daily kms</p>
                  <p class="content is-small">Kilometers required per day to reach the
                    goal by 2019-12-31.</p>
                  <p class="content is-large">{{ Math.round(((5000 - kms)/(365 - getDayOfYear())) * 100 )/ 100 }} km/days remaining</p>
                </article>
              </div>
            </div>
            </div>
            `
})


const portfolio = Vue.component('portfolio', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
            <div>
                <section class="hero is-info">
                  <div class="hero-body">
                    <div class="container">
                      <h1 class="title">
                        Portfolio
                      </h1>
                      <h2 class="subtitle">
                        Some of my work
                      </h2>
                    </div>
                  </div>
                </section>
                  <p>Below you'll find some of my projects, click them for more information. Please visit my repositories on
                  <a href="https://github.com/JohanCederschiold">Github</a> or
                  <a href="https://gitlab.com/JohanCederschiold">GitLab</a> if you are curious about my other projects.</p>

                  <a><port-turtle></port-turtle></a>
                  <a><port-quiz></port-quiz></a>
                  <a><port-docker></port-docker></a>
            </div>
             `
})

const dev = Vue.component('dev-frames', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
            <div>
              <section class="hero is-info">
                <div class="hero-body">
                  <div class="container">
                    <h1 class="title">
                      Development Frameworks
                    </h1>
                    <h2 class="subtitle">
                      Ways to work
                    </h2>
                  </div>
                </div>
              </section>
              <p>We have been studying Agile development methods focusing on the Scrum framework. We did a project where we where to work a sprint as a team to get to convert theory to practice. We have also touched on Pair programming and Kanban.</p>
              <p>In my previous work experience I’ve been working with the waterfall model in several projects.</p>
            </div>
             `
})

Vue.component('port-turtle', {
  data() {
    return {
      showMe: 'modal'
    }
  },
  methods: {
    showMore(){
      if (this.showMe === 'modal') {
        this.showMe = 'modal is-active'
      } else {
        this.showMe = 'modal'
      }
    }
  },
  template: `<div>
                <article class="message is-info" v-on:click="showMore">
                <div class="message-header">
                  <p>Turtlerace</p>
                </div>
                <div class="message-body">
                  Why not race turtles and populate tables at the same time?
                </div>
                <div>
                <a class="button is-warning is-read-more">Read more</a>
                </div>
              </article>
                <div v-bind:class="showMe">
                  <div class="modal-background"></div>
                  <div class="modal-card">
                    <header class="modal-card-head">
                      <p class="modal-card-title">Turtlerace</p>
                      <button class="delete" aria-label="close" v-on:click="showMore"></button>
                    </header>
                    <section class="modal-card-body">
                      <div>
                        <p>My intent with this was originally to create a servlet that auto populated a database with data, in several tables, for me to work with, both on the server-side and the client-side.</p>
                        <p>The servlet runs a race (with race-turtles…of course) every ten minutes and the results are stored in three tables (turtle, race and raceresults).</p>
                        <p>The project grew to where I wanted to follow best practice in terms of Normalization of the database and keeping with industry standard in how requests are made to the database (the servlet make the requests via an interface).</p>
                        <p>Via a browser, CURL, insomnia etc the database can be queried about registered turtles, latest race results and the current leader board.</p>
                        <p>This project is created with docker-compose.</p>
                      </div>
                    </section>
                    <footer class="modal-card-foot">
                      <a href="https://github.com/JohanCederschiold/Turtlerace_docker_mvn"><button class="button is-success">Go to project on github</button></a>
                      <button class="button" v-on:click="showMore">Cancel</button>
                    </footer>
                  </div>
                </div>
              </div>`
})

Vue.component('port-quiz', {
  data() {
    return {
      showMe: 'modal'
    }
  },
  methods: {
    showMore(){
      if (this.showMe === 'modal') {
        this.showMe = 'modal is-active'
      } else {
        this.showMe = 'modal'
      }
    }
  },
  template: `<div>
                <article class="message is-info" v-on:click="showMore">
                <div class="message-header">
                  <p>Quizmaster</p>
                </div>
                <div class="message-body">
                  A tool to quiz me for the tests of last fall.
                </div>
                <div>
                <a class="button is-warning is-read-more">Read more</a>
                </div>
              </article>
                <div v-bind:class="showMe">
                  <div class="modal-background"></div>
                  <div class="modal-card">
                    <header class="modal-card-head">
                      <p class="modal-card-title">Quizmaster</p>
                      <button class="delete" aria-label="close" v-on:click="showMore"></button>
                    </header>
                    <section class="modal-card-body">
                      <div>
                      <p>One of my earliest projects on Github, Quizmaster, was initially just an exercise in reading text files with Java and working with version control with git.</p>
                      <p>In the process I realized I could use the program to Quiz myself on the upcoming tests in the fall / winter of 2018. Eventually I released the app to my classmates who also started using it.</p>
                      <p>The program parsed a text-file comprising both-question and answers (semi-colon separated). It the proceeded putting the questions to the user in random order. Questions that where answered correctly would not appear again (within the same round). Incorrectly answered questions would reappear until correctly answered or the user terminated the round.</p>
                      <p>We started a second repository where other students in Java18 could fork and contribute to the question bank.</p>
                      </div>
                    </section>
                    <footer class="modal-card-foot">
                      <a href="https://github.com/JohanCederschiold/Quizmaster"><button class="button is-success">Go to project on github</button></a>
                      <button class="button" v-on:click="showMore">Cancel</button>
                    </footer>
                  </div>
                </div>
              </div>`
})

Vue.component('port-docker', {
  data() {
    return {
      showMe: 'modal'
    }
  },
  methods: {
    showMore(){
      if (this.showMe === 'modal') {
        this.showMe = 'modal is-active'
      } else {
        this.showMe = 'modal'
      }
    }
  },
  template: `<div>
                  <article class="message is-info" v-on:click="showMore">
                  <div class="message-header">
                    <p>Docker-compose</p>
                  </div>
                  <div class="message-body">
                    A docker project for school using Wildfly, NGINX and PostgreSQL
                  </div>
                  <div>
                  <a class="button is-warning is-read-more">Read more</a>
                  </div>
                </article>
                  <div v-bind:class="showMe">
                    <div class="modal-background"></div>
                    <div class="modal-card">
                      <header class="modal-card-head">
                        <p class="modal-card-title">Docker-compose</p>
                        <button class="delete" aria-label="close" v-on:click="showMore"></button>
                      </header>
                      <section class="modal-card-body">
                        <div>
                          The purpose of the project was to create a docker-compose project consisting of:
                          <ul>
                            <li>Wildfly running a servlet and static content.</li>
                            <li>The servlet should be able to act on POST and GET.</li>
                            <li>PostgreSQL (storing from POST and serving GET).</li>
                          </ul>
                          For extra credit:
                          <ul>
                            <li>NGINX acting as reversed proxy.</li>
                            <li>Enable HTTPS connections (with self-signed TSL certificate).</li>
                          </ul>
                          The project was to be packaged in such a way that the user should be able to run “docker-compose up” (assuming the necessary software (docker, docker-compose) and port forwarding, if running in a VM, was in place).
                        </div>
                      </section>
                      <footer class="modal-card-foot">
                        <a href="https://gitlab.com/JohanCederschiold/docker_postgres_wildfly"><button class="button is-success">Go to project on gitlab</button></a>
                        <button class="button" v-on:click="showMore">Cancel</button>
                      </footer>
                    </div>
                  </div>
              </div>`
})

const docker = Vue.component('docker', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
              <div>
                  <section class="hero is-info">
                    <div class="hero-body">
                      <div class="container">
                        <h1 class="title">
                          Tools
                        </h1>
                        <h2 class="subtitle">
                          Docker
                        </h2>
                      </div>
                    </div>
                  </section>
                  <p>In the Development-Tools course we have been trained in the practical use of Docker and Docker-Compose. Since the completion of the course we’ve used Docker and Docker-compose in several projects. </p>
                  <p>We have experienced the advantages of a docker-based project firsthand in our coop-exercises. Since basically all students have a different setup in terms of OS, hardware and environment variables Docker was fundamental in easing cooperation within the teams. </p>
             </div>
             `
})

const maven = Vue.component('maven', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
              <div>
                <section class="hero is-info">
                  <div class="hero-body">
                    <div class="container">
                      <h1 class="title">
                        Tools
                      </h1>
                      <h2 class="subtitle">
                        Maven
                      </h2>
                    </div>
                  </div>
                </section>
                  <p>We have used Maven as a build tool in many of the school-projects and done some work with integrated tests (JUnit & GitLab).</p>
             </div>
             `
})

const git = Vue.component('git', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
              <div>
                <section class="hero is-info">
                  <div class="hero-body">
                    <div class="container">
                      <h1 class="title">
                        Tools
                      </h1>
                      <h2 class="subtitle">
                        Git
                      </h2>
                    </div>
                  </div>
                </section>
                  <p>Version Control with GIT has been an essential tool (and the source of some headache) in all coop exercises of the course. I’m currently using GIT daily in both school-projects and my own ventures..</p>
             </div>
             `
})

const vm = Vue.component('vm', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
              <div>
              <section class="hero is-info">
                <div class="hero-body">
                  <div class="container">
                    <h1 class="title">
                      Tools
                    </h1>
                    <h2 class="subtitle">
                      Virtualization
                    </h2>
                  </div>
                </div>
              </section>
                <p>Virtualization has been key in several areas of our training. We’ve made extensive use of Vagrant for temporary development environments and to get training on Linux-based operating systems (mainly Debian and Fedora).</p>
                <p>Vagrant has been essential in enabling the use of docker on windows (without a Windows pro license).
                We have done basic configuration of NGINX and Wildfly, both in Docker or directly in a Virtual Machine acting as server. </p>
                <p>We have worked with VMWare and VirtualBox</p>
             </div>
             `
})

const frontend = Vue.component('frontend', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
            <div>
            <section class="hero is-info">
              <div class="hero-body">
                <div class="container">
                  <h1 class="title">
                    Languages
                  </h1>
                  <h2 class="subtitle">
                    Frontend frameworks
                  </h2>
                </div>
              </div>
            </section>
              <p>I have basic training in Frontend technologies such as HTML, CSS and JavaScript. We’ve done projects both vanilla JavaScript and the Vue-framework. This webpage is a test-case.</p>
            </div>
            `
})

const java = Vue.component('java', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
            <div>
            <section class="hero is-info">
              <div class="hero-body">
                <div class="container">
                  <h1 class="title">
                    Languages
                  </h1>
                  <h2 class="subtitle">
                    Java
                  </h2>
                </div>
              </div>
            </section>
              <p>Java is at the core of the education. The projects have focused on Server-applications. We’ve worked with JPA as well as JDBC. The server-applications are primarily accessible via REST and/or SOAP. Have also programmed clients with RMI.</p>
            </div>
            `
})

const sql = Vue.component('sql', {
  created() {
    this.$store.commit('showingContent', true)
  },
  template: `
            <div>
              <section class="hero is-info">
                <div class="hero-body">
                  <div class="container">
                    <h1 class="title">
                      Tools
                    </h1>
                    <h2 class="subtitle">
                      SQL
                    </h2>
                  </div>
                </div>
              </section>
              <p>We’ve been doing extensive work with databases. First and foremost, we have been working with MySQL/MariaDB and SQLServer. But we’ve also done projects with SQLite, DB Derby and PostgreSQL.</p>
              <p>
              The focus has been on getting an understanding of the basic structure of relational databases in general and best practice, i.e. Normalization.
              </p>
            </div>
            `
})



const ph = Vue.component('placeHolder', {
  // created() {
  //   this.$store.commit('showingContent', true)
  // },
  data() {
    return {
    }
  },
  methods: {
  },
  template: `<div>
                <figure>
                  <img src="bridge-small.jpg" alt="A bridge over not so troubled water!">
                </figure>
             </div>`
})

const home = Vue.component('home', {
  created() {
    this.$store.commit('showingContent', false)
  },
  template: `<div></div>`
})

Vue.component('mynavbar', {
  created() {
    console.log('Navbar created')
  },
  data() {
    return {
    }
  },
  methods: {
  },
  template: `
            <div>
            <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-menu is-active">
            <div class="navbar-start">
            <a class="navbar-item">
            <router-link to="/about">About</router-link>
            </a>
            <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link">
            Tools
            </a>

            <div class="navbar-dropdown">
            <a class="navbar-item">
            <router-link to="/tools/docker">Docker</router-link>
            </a>
            <a class="navbar-item">
            <router-link to="/tools/maven">Maven</router-link>
            </a>
            <a class="navbar-item">
            <router-link to="/tools/git">Git</router-link>
            </a>
            <a class="navbar-item">
            <router-link to="/tools/virtualization">Virtualization</router-link>
            </a>
            </div>
            </div>

            <a class="navbar-item">
            <router-link to="/development_frameworks">Development Frameworks</router-link>
            </a>


            <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link">
            Languages
            </a>

            <div class="navbar-dropdown">
            <a class="navbar-item">
            <router-link to="/languages/java">Java</router-link>
            </a>
            <a class="navbar-item">
            <router-link to="/languages/sql">SQL</router-link>
            </a>
            <a class="navbar-item">
            <router-link to="/languages/frontend">Frontend</router-link>
            </a>
            </div>
            </div>
            <a class="navbar-item">
            <router-link to="/portfolio">Portfolio</router-link>
            </a>
            </div>

            <div class="navbar-end">
            </div>
            </div>
            </nav>

            </div>


            </div>
  `
})

const reportKm = Vue.component('reportKm', {
  created() {
    this.$store.commit('showingContent', true)
  },
  methods: {
    registerKms(){
      let kms = document.querySelector('#mykms').value
      if (isNaN(kms)) {
        document.querySelector('#mykms').value = "Please use an integer as input"
      } else {
        // Converts floats (if present) to integers.
        let myInteger = parseInt((Number(kms) + 0.5), 10)
        console.log(myInteger)
        this.sendToDb(myInteger)
      }
    },
    clearText() {
      document.querySelector('#mykms').value = ""
    },
    sendToDb(kms) {
      // Skapa body för form encoded body
      let bodyData = "distance=" + kms
      fetch('http://95.217.9.10/api', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: bodyData
      }).then(response => this.feedbackOnStatus(response))
    },
    feedbackOnStatus(response) {
      const kmsField = document.querySelector('#mykms')
      if (response.status === 200) {
        kmsField.value = "Your distance has been logged"
      }
    }
  },
  template: `
              <div>
                <section class="hero is-info">
                  <div class="hero-body">
                    <div class="container">
                      <h1 class="title">
                        Register distance
                      </h1>
                      <h2 class="subtitle">
                        Kilometers ytd
                      </h2>
                    </div>
                  </div>
                </section>
                <div class="field">
                  <label class="label">Kilometers</label>
                  <div class="control">
                    <input class="input" id="mykms" type="text" placeholder="Text input">
                  </div>
                </div>
                <div class="field is-grouped">
                <div class="control">
                  <button class="button is-link" v-on:click="registerKms">Register</button>
                </div>
                <div class="control">
                  <button class="button is-text" v-on:click="clearText">Clear</button>
                </div>
              </div>
            </div>
            `
})

Vue.component('myFooter', {
  data() {
    return {
    }
  },
  methods: {
  },
  template: `<div>
              <footer class="footer">
                <div class="content has-text-centered">
                  <p>
                    <strong>Johan Cederschiöld</strong>
                    <div class="content is-large">
                      <a href="https://linkedin.com/in/johan-cederschiold"><i class="fab fa-linkedin"></i></a>
                      <a href="https://www.instagram.com/JuanProtector/"><i class="fab fa-instagram"></i></a>
                      <a href="mailto:johan.cederschiold@gmail.com"><i class="far fa-envelope"></i></a>
                      <a href="https://github.com/JohanCederschiold"><i class="fab fa-github-square"></i></a>
                      <a href="https://gitlab.com/JohanCederschiold"><i class="fab fa-gitlab"></i></a>
                    </div>

                  </p>
                </div>
              </footer>
            </div>`
})

Vue.component('App', {
  created() {
    this.$store.commit('showingContent', false )
    console.log('App-created')
  },
  template: `<div>
  <thecontent></thecontent>
  <placeHolder v-if="this.$store.state.currentChoice!==true"></placeHolder>
  <myFooter></myFooter>
  </div>`
})

const state = {
  currentChoice: null
}

const mutations = {
  showingContent(state, choice) {
    state.currentChoice = choice
    console.log(choice)
  }
}
const store = new Vuex.Store({
  mutations,
  state
})

const router = new VueRouter({
  routes: [{
    component: about,
    path: '/about'
  }, {
    component: portfolio,
    path: '/portfolio'
  }, {
    component: dev,
    path: '/development_frameworks'
  }, {
    component: dev-frames,
    path: '/development_frameworks'
  }, {
    component: java,
    path: '/languages/java'
  }, {
    component: sql,
    path: '/languages/sql'
  }, {
    component: frontend,
    path: '/languages/frontend'
  }, {
    component: docker,
    path: '/tools/docker'
  }, {
    component: vm,
    path: '/tools/virtualization'
  }, {
    component: maven,
    path: '/tools/maven'
  }, {
    component: git,
    path: '/tools/git'
  },
  {
    component: reportKm,
    path: '/report'
  },
  {
    component: stats,
    path: '/stats'
  },
  {
    component: home,
    path: '/'
  }
]
})


new Vue({
  el: '#app',
  store,
  router
})
